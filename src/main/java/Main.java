import org.nuiton.pollux.Pollux;
import picocli.CommandLine;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        CommandLine.call(new Pollux(), args);
    }
}
