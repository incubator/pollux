package org.nuiton.pollux.model;

import java.util.List;

public class ModelClass extends ModelType {

    public boolean isAbstract;
    public boolean isInterface;
    public List<ModelTypeRef> extendedTypes;

    public ModelClass(String name, List<ModelTypeRef> typeParameters, boolean isAbstract, boolean isInterface, List<ModelTypeRef> extendedTypes, List<ModelTypeRef> implementedTypes) {
        super(name, typeParameters, implementedTypes);
        this.name = name;
        this.isAbstract = isAbstract;
        this.isInterface = isInterface;
        this.extendedTypes = extendedTypes;
    }

}
