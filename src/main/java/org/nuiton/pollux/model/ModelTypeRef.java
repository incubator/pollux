package org.nuiton.pollux.model;

import java.util.List;

public class ModelTypeRef {

    public String name;
    public List<ModelTypeRef> parameters;

    public ModelTypeRef(String name, List<ModelTypeRef> modelSubTypes) {
        this.name = name;
        this.parameters = modelSubTypes;
    }

}
