package org.nuiton.pollux.model;

import java.util.LinkedList;
import java.util.List;

public class ModelType {

    public String name;
    public List<ModelTypeRef> parameters;
    public List<ModelTypeRef> implementedTypes;
    public List<ModelAttribute> attributes = new LinkedList<>();

    public ModelType(String name, List<ModelTypeRef> parameters, List<ModelTypeRef> implementedTypes) {
        this.name = name;
        this.parameters = parameters;
        this.implementedTypes = implementedTypes;
    }

}
