package org.nuiton.pollux.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ModelEnum extends ModelType {

    public List<ModelEnumValue> values = new LinkedList<>();

    public ModelEnum(String name, List<ModelTypeRef> implementedTypes) {
        super(name, Collections.emptyList(), implementedTypes);
    }

}
