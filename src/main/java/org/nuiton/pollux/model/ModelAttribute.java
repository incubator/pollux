package org.nuiton.pollux.model;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class ModelAttribute {

    private static Set<String> STANDARD_CONTAINER_TYPES = ImmutableSet.of(
            "List",
            "Collection",
            "Map",
            "Set",
            "Stack"); // TODO et toutes les classes concretes !

    public String name;
    public ModelType modelType;

    public ModelAttribute(String name, ModelType modelType) {
        this.name = name;
        this.modelType = modelType;
    }

    public boolean standardContainer() {
        return STANDARD_CONTAINER_TYPES.contains(modelType.name);
    }
}
