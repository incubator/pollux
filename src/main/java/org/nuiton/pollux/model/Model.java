package org.nuiton.pollux.model;

import java.util.HashMap;
import java.util.Map;

public class Model {

    public Map<String, ModelType> typesByFullyQualifiedName = new HashMap<>();

    public void addType(ModelType modelType) {
        typesByFullyQualifiedName.put(modelType.name, modelType);
    }

    public boolean resolve(String type) {
        return typesByFullyQualifiedName.containsKey(type);
    }

    public ModelType getModelType(String className) {
        return typesByFullyQualifiedName.get(className);
    }
}
