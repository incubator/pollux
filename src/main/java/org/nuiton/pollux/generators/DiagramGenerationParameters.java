package org.nuiton.pollux.generators;

public class DiagramGenerationParameters {

    public String title;
    public boolean straitLines;
    public Integer maxEnumValues;

    public DiagramGenerationParameters(String title, boolean straitLines, Integer maxEnumValues) {
        this.title = title;
        this.straitLines = straitLines;
        this.maxEnumValues = maxEnumValues;
    }
}
