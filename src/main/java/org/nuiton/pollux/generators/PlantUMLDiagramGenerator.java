package org.nuiton.pollux.generators;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.nuiton.pollux.model.*;

public class PlantUMLDiagramGenerator {

    public String generateDiagram(DiagramGenerationParameters diagramGenerationParameters, Model model) {
        StringBuilder diagram = new StringBuilder();

        diagram.append("@startuml\n\n");

        if (diagramGenerationParameters.straitLines) {
            diagram.append("skinparam linetype ortho\n\n");
        }

        if (diagramGenerationParameters.title != null) {
            diagram.append("title ").append(diagramGenerationParameters.title).append("\n\n");
        }

        // Entités
        for (ModelType modelType : model.typesByFullyQualifiedName.values()) {
            // Déclaration du type
            generateTypeDeclaration(diagram, modelType);
            diagram.append(" {\n");

            if (modelType instanceof ModelEnum) {
                // Valeurs de l'enum
                generateEnumValues(diagramGenerationParameters, diagram, (ModelEnum) modelType);
            }
            // Attributs sous forme de champs
            generateAttributesAsFields(model, diagram, modelType);

            diagram.append("}\n");
        }
        diagram.append("\n");

        // Associations
        for (ModelType modelType : model.typesByFullyQualifiedName.values()) {
            // Heritage
            if (modelType instanceof ModelClass) {
                generateInheritImplements(diagram, modelType, ((ModelClass) modelType).extendedTypes, " <|-- ");
            }
            // Implementation
            generateInheritImplements(diagram, modelType, modelType.implementedTypes, " <|.. ");
            // Attributs sous forme d'associations
            generateAttributesAsAssociations(model, diagram, modelType);
        }
        diagram.append("\n");

        diagram.append("@enduml\n");

        return diagram.toString();
    }

    private void generateTypeDeclaration(StringBuilder diagram, ModelType modelType) {
        String typeName;
        if (modelType instanceof ModelEnum) {
            typeName = "enum";
        } else {
            ModelClass modelClass = (ModelClass) modelType;
            typeName = modelClass.isInterface ? "interface" : modelClass.isAbstract ? "abstract class" : "class";
        }
        diagram.append(typeName).append(" ").append(modelType.name);
        if (modelType.parameters.size() > 0) {
            diagram.append("<").append(modelType.parameters.stream().map(t -> t.name).collect(joining(", "))).append(">");
        }
    }

    private void generateEnumValues(DiagramGenerationParameters diagramGenerationParameters, StringBuilder diagram, ModelEnum modelEnum) {
        List<ModelEnumValue> values = modelEnum.values;
        int limit = values.size();

        // Limitation du nb de lignes affichées à partir de la limite indiquée + 50% de tolérance
        if (diagramGenerationParameters.maxEnumValues != null) {
            if (values.size() > diagramGenerationParameters.maxEnumValues * 1.5) {
                limit = diagramGenerationParameters.maxEnumValues;
            }
        }

        for (ModelEnumValue value : values.subList(0, limit)) {
            diagram.append("  ").append(value.name).append("\n");
        }
        if (limit < values.size()) {
            diagram.append("< and more >\n");
        }
    }

    private void generateAttributesAsFields(Model model, StringBuilder diagram, ModelType modelType) {
        List<ModelAttribute> attributes = modelType.attributes;
        for (ModelAttribute attribute : attributes) {
            if (!typeInModel(model, attribute).isPresent()) {
                diagram.append("  ").append(attribute.modelType.name).append(" ").append(attribute.name).append("\n");
            }
        }
    }

    private void generateInheritImplements(StringBuilder diagram, ModelType modelType, List<ModelTypeRef> extendedTypes, String s) {
        for (ModelTypeRef extendedType : extendedTypes) {
            diagram.append(extendedType.name).append(s).append(modelType.name);
            List<ModelTypeRef> parameters = extendedType.parameters;
            if (parameters.size() > 0) {
                diagram.append(" : ").append("<").append(parameters.stream().map(t -> t.name).collect(joining(", "))).append(">");
            }
            diagram.append("\n");
        }
    }

    private void generateAttributesAsAssociations(Model model, StringBuilder diagram, ModelType modelType) {
        for (ModelAttribute attribute : modelType.attributes) {
            Optional<String> typeInModel = typeInModel(model, attribute);
            if (typeInModel.isPresent()) {
                String type = typeInModel.get();
                String cardinality = attribute.standardContainer() ? "\"0..*\"" : "\"0..1\"";
                diagram.append(modelType.name).append(" -->" + cardinality + " ").append(type).append(" : ").append(attribute.name).append("\n");
            }
        }
    }

    private Optional<String> typeInModel(Model model, ModelAttribute attribute) {
        List<String> types = new LinkedList<>();
        types.add(attribute.modelType.name);
        if (attribute.modelType.parameters != null) {
            types.addAll(attribute.modelType.parameters.stream().map(t -> t.name).collect(toList()));
        }

        return types.stream().filter(model::resolve).findFirst();
    }

}
