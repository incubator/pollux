package org.nuiton.pollux.parsers;

import org.nuiton.pollux.model.Model;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;

public class JavaParser {

    public Model parseJavaSources(Collection<Path> sources) throws IOException {
        Model model = new Model();

        ASTVisitor ASTVisitor = new ASTVisitor();
        ASTVisitorContext astVisitorContext = new ASTVisitorContext(model);
        for (Path source : sources) {
            ASTVisitor.visit(com.github.javaparser.JavaParser.parse(source), astVisitorContext);
        }

        return model;
    }

}
