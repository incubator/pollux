package org.nuiton.pollux.parsers;

import java.util.Stack;

import org.nuiton.pollux.model.Model;
import org.nuiton.pollux.model.ModelClass;
import org.nuiton.pollux.model.ModelType;

public class ASTVisitorContext {

    public Model model;
    public Stack<String> scopes = new Stack<>();

    public ASTVisitorContext(Model model) {
        this.model = model;
    }

    public void enterScope(String ScopeName) {
        scopes.push(ScopeName);
    }

    public ModelType getCurrentScope() {
        return scopes.isEmpty() ? null : model.getModelType(scopes.peek());
    }

    public void exitScope(String ScopeName) {
        scopes.pop();
    }

}
