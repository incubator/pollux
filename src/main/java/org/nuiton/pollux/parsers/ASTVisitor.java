package org.nuiton.pollux.parsers;

import static java.util.stream.Collectors.toList;

import java.util.LinkedList;
import java.util.List;

import org.nuiton.pollux.model.*;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.EnumConstantDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.ast.visitor.GenericVisitorAdapter;

public class ASTVisitor extends GenericVisitorAdapter<Void, ASTVisitorContext> {

    @Override
    public Void visit(ClassOrInterfaceDeclaration d, ASTVisitorContext context) {
        ModelType currentScope = context.getCurrentScope();
        String className = (currentScope == null ? "" : currentScope.name + "_") + d.getName().toString();
        boolean isAbstract = d.isAbstract();
        boolean isInterface = d.isInterface();

        List<ModelTypeRef> typeParameters = d.getTypeParameters().stream().map(t -> new ModelTypeRef(t.getName().toString(), null)).collect(toList());
        List<ModelTypeRef> extendedTypes = d.getExtendedTypes().stream().map(t -> toModelTypeRef(t, context)).collect(toList());
        List<ModelTypeRef> implementedTypes = d.getImplementedTypes().stream().map(t -> toModelTypeRef(t, context)).collect(toList());

        context.model.addType(new ModelClass(className, typeParameters, isAbstract, isInterface, extendedTypes, implementedTypes));

        context.enterScope(className);
        super.visit(d, context);
        context.exitScope(className);

        return null;
    }

    private ModelTypeRef toModelTypeRef(ClassOrInterfaceType type, ASTVisitorContext context) {
        ModelType currentScope = context.getCurrentScope();

        List<ModelTypeRef> modelSubTypes = new LinkedList<>();
        NodeList<Type> subTypes = type.getTypeArguments().orElse(null);
        if (subTypes != null) {
            for (Type subType : subTypes) {
                if (subType instanceof ClassOrInterfaceType) {
                    modelSubTypes.add(toModelTypeRef((ClassOrInterfaceType) subType, context));
                } else {
                    System.out.println(subType);
                }
            }
        }
        String name = (currentScope == null ? "" : currentScope.name + "_") + type.getName().toString();
        return new ModelTypeRef(name, modelSubTypes);
    }

    @Override
    public Void visit(EnumDeclaration d, ASTVisitorContext context) {
        ModelType currentScope = context.getCurrentScope();
        String enumName = (currentScope == null ? "" : currentScope.name + "_") + d.getName().toString();

        List<ModelTypeRef> implementedTypes = d.getImplementedTypes().stream().map(t -> toModelTypeRef(t, context)).collect(toList());

        ModelEnum modelEnum = new ModelEnum(enumName, implementedTypes);
        context.model.addType(modelEnum);

        context.enterScope(enumName);
        super.visit(d, context);
        context.exitScope(enumName);

        return null;
    }

    @Override
    public Void visit(EnumConstantDeclaration d, ASTVisitorContext context) {
        ModelEnum theEnum = ModelEnum.class.cast(context.getCurrentScope());
        String enumValueName = d.getName().toString();

        theEnum.values.add(new ModelEnumValue(enumValueName));

        super.visit(d, context);

        return null;
    }

    @Override
    public Void visit(FieldDeclaration d, ASTVisitorContext context) {
        ModelType currentScope = context.getCurrentScope();

        NodeList<VariableDeclarator> variables = d.getVariables();
        for (VariableDeclarator variable : variables) {
            Type variableType = variable.getType();
            String name = variable.getName().asString();
            String type;
            List<ModelTypeRef> typeParameters = null;
            if (variableType instanceof ClassOrInterfaceType) {
                ClassOrInterfaceType classOrInterfaceType = (ClassOrInterfaceType) variableType;
                type = classOrInterfaceType.getName().asString();
                typeParameters = classOrInterfaceType.getTypeArguments().orElse(new NodeList<>()).stream()
                        .map(Node::toString)
                        .map(t -> {
                            List<ModelType> implementedTypes = null; // TODO
                            return new ModelTypeRef(t, null);
                        }) // TODO gérer les parametersTypes imbriqués
                        .collect(toList());
            } else {
                type = variableType.asString();
            }
            currentScope.attributes.add(new ModelAttribute(name, new ModelType(type, typeParameters, null))); // TODO
        }

        super.visit(d, context);

        return null;
    }
}