package org.nuiton.pollux;

import org.nuiton.pollux.generators.DiagramGenerationParameters;
import org.nuiton.pollux.generators.PlantUMLDiagramGenerator;
import org.nuiton.pollux.model.Model;
import org.nuiton.pollux.parsers.JavaParser;
import picocli.CommandLine;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

public class Pollux implements Callable<Void> {

    @CommandLine.Parameters(index = "0", paramLabel = "OUTPUT_FILE_OR_DIRECTORY", description = "Generated diagram output file (or directory).")
    private Path outputFile;

    @CommandLine.Parameters(index = "1..*", arity = "1..*", paramLabel = "INPUT_FILES_AND_DIRECTORIES", description = "File(s) and directory(ies) to process (recursively).")
    private Path[] inputFiles;

    @CommandLine.Option(names = {"-t", "--title"}, description = "Diagram title")
    String title;

    @CommandLine.Option(names = {"--straight-lines"}, description = "Use only straight lines for diagram links.")
    private boolean straitLines = false;

    @CommandLine.Option(names = {"--max-enum-values"}, description = "Limit displayed enum values (with a +50% tolerance.")
    private Integer maxEnumValues;

    @Override
    public Void call() throws Exception {
        Set<Path> javaFiles = new HashSet<>();
        for (Path inputFile : inputFiles) {
            if (Files.isRegularFile(inputFile) && inputFile.toString().endsWith("java")) {
                javaFiles.add(inputFile);
            } else if (Files.isDirectory(inputFile)) {
                javaFiles.addAll(Files.walk(inputFile)
                        .filter(Files::isRegularFile)
                        .filter(f -> f.toString().endsWith("java"))
                        .collect(Collectors.toSet()));
            } else {
                System.out.println("Ignoring non java, non directory input file " + inputFile);
            }
        }

        Model model = new JavaParser().parseJavaSources(javaFiles);
        String diagram = new PlantUMLDiagramGenerator().generateDiagram(new DiagramGenerationParameters(title, straitLines, maxEnumValues), model);

        if (Files.isDirectory(outputFile)) {
            outputFile = outputFile.resolve((title == null ? LocalDateTime.now().toString() : title) + ".puml");
        }
        Files.write(outputFile, singletonList(diagram), Charset.forName("UTF-8"));

        return null;
    }
}
