package org.nuiton.pollux;

import org.junit.jupiter.api.Test;
import org.nuiton.pollux.generators.DiagramGenerationParameters;
import org.nuiton.pollux.model.Model;
import org.nuiton.pollux.parsers.JavaParser;
import org.nuiton.pollux.generators.PlantUMLDiagramGenerator;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.file.Paths.get;
import static java.util.Collections.singletonList;

class PlantUMLDiagramGeneratorTest {

    public static final String SOURCE = "src/test/java/org/nuiton/pollux/model/";
    public static final String TITLE = "Modèle de test";

    @Test
    public void generateDiagram() throws IOException {
        List<Path> javaFiles = Files.walk(get(SOURCE))
                .filter(Files::isRegularFile)
                .filter(f -> f.toString().endsWith("java"))
                .collect(Collectors.toList());

        Model model = new JavaParser().parseJavaSources(javaFiles);
        String diagram = new PlantUMLDiagramGenerator().generateDiagram(new DiagramGenerationParameters(TITLE, false, null), model);

        Files.write(get(SOURCE + TITLE + ".puml"), singletonList(diagram), Charset.forName("UTF-8"));
    }

}